#include <SD.h>

//SPI Settings
//MOSI, MISO, SCLK set by default
int CS_pin = 10;
int power_pin = 8;

void setup() {
  Serial.begin(9600);
  //CS pin output
  pinMode(CS_pin, OUTPUT);

  //CS draw power from pin 8

  pinMode(power_pin, OUTPUT);
  digitalWrite(power_pin, HIGH);

  //Check card ready

  if(!SD.begin(CS_pin))
  { 
    Serial. println("Card Failed");
    return;
  }
  Serial.println("Card Ready");

}

void loop() {
String dataString = "It works";
//Open file to write
//Open one file at a time
File dataFile = SD.open("testing.txt", FILE_WRITE); 
if(dataFile)
{
  dataFile.println(dataString);
  dataFile.close(); 
  Serial.println(dataString); 
}
else 
{
  Serial.println("Couldn't access file");
}

delay(5000); 

}
