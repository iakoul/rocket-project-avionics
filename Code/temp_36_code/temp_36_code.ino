
float mode_tempC(int sensorpin){
  // read multiple values and sort them to take the mode
  int sortedValues[100];
  for(int i=0;i<100;i++){
    int value_temp = analogRead(sensorpin);
    int j;
    if(value_temp<sortedValues[0] || i==0){
      j=0; //insert at first position
    }
    else{
      for(j=1;j<i;j++){
        if(sortedValues[j-1]<=value_temp && sortedValues[j]>=value_temp){
          // j is insert position
          break;
        }
      }
    }
    for(int k=i;k>j;k--){
      // move all values higher than current reading up one position
      sortedValues[k]=sortedValues[k-1];
    }
    sortedValues[j]=value_temp; //insert current reading
  }
  //return scaled mode of 10 values
  float returnval = 0;
  for(int i=45;i<55;i++){
    returnval +=sortedValues[i];
  }
  returnval = returnval/10;
  float voltage = returnval * 5/1024; //Readtemperature filters analog readings
  float temp = ((voltage - 0.5) *100); //calculates the temo in C
  return temp;
}


void thermalcontrol(){
  float realtemp;
 /* if (launch==1 && land==0){ //start from launch end at land
    if (currenttime-tmppretime>=tmpupdatetime){
      tmppretime=currenttime;
      // read voltage////////////////////////////
      mainread=analogRead(A0);
      mainpower=(mainread*15)/1023;
      Serial.print("T,");
      Serial.print(currenttime);
      Serial.print(",");
      Serial.print(mainpower);
      Serial.print(","); */
      tmpsteadyfilter();
 /*     
      if (mainpower>criticalvoltage){
        if (realtemp<criticaltemp){
          digitalWrite(heatingpin,HIGH);
        }
        else {
          digitalWrite(heatingpin,LOW);
        }

      }
      else {
        Serial.print("Voltage too low");
      } //debug only
    }
  }*/
}

float tmpsteadyfilter(){
  float realtemp, currenttemp, pretemp;
  int sensorpin = 1;
  currenttemp = mode_tempC(sensorpin);
  if (abs(currenttemp-pretemp)>0.8){
    realtemp=currenttemp;
    pretemp=currenttemp;  
  }
  else {
    realtemp=pretemp;
  }
  return realtemp;
}

void setup()
{
  Serial.begin(9600);
}

void loop() 
{
  int LED = 7; 
  pinMode(LED, OUTPUT);
  float temp, tempF;
  thermalcontrol();
  temp = tmpsteadyfilter();
  Serial.print("Temperature is: "); Serial.println(temp);
  tempF = (temp*9/5) + 32.0;
  Serial.print("Temperature in Fahrenheit: "); Serial.println(tempF);
  Serial.println("");
  
  if( tempF > 77){
    digitalWrite(LED, HIGH);
    Serial.println("LED ON!");
    delay(1000);
    digitalWrite(LED, LOW);
}
  else
    digitalWrite(LED,LOW);
    
  delay(1000);
}

